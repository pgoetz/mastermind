package de.pgoetz.mastermind;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Random;

public class Mastermind {
    private static final int CODE_LENGTH = 4;
    private static final char[] CODE_LETTERS = {'A', 'B', 'C', 'D', 'E', 'F'};
    public static void main(String[] args) {
        new Mastermind().play();
    }

    public void play() {
        Validator validator = new Validator(createCode());
        BufferedReader console = new BufferedReader(new InputStreamReader(System.in));

        intro();

        try {
            for (int i = 0; i < 8; i++) {
                System.out.printf("Guess %d%n: ", i + 1);
                String guess = console.readLine();
                String feedback = validator.validate(guess);

                if (feedback.equals(Validator.CORRECT_GUESS)) {
                    win();
                } else {
                    System.out.println(feedback);
                }
            }

            lose();
        } catch (IOException e) {
            e.printStackTrace();

            try {
                console.close();
            } catch (IOException ex) {
                e.printStackTrace();
            }
        }
    }

    private static String createCode() {
        StringBuilder codeBuilder = new StringBuilder();
        Random random = new Random();

        for(int i = 0; i < Mastermind.CODE_LENGTH; i++) {
            codeBuilder.append(Mastermind.CODE_LETTERS[random.nextInt(Mastermind.CODE_LETTERS.length)]);
        }

        return codeBuilder.toString();
    }

    private void intro() {
        System.out.println("Welcome to");
        System.out.println("    __  ___           __                      _           __");
        System.out.println("   /  |/  /___ ______/ /____  _________ ___  (_)___  ____/ /");
        System.out.println("  / /|_/ / __ `/ ___/ __/ _ \\/ ___/ __ `__ \\/ / __ \\/ __  /");
        System.out.println(" / /  / / /_/ (__  ) /_/  __/ /  / / / / / / / / / / /_/ /");
        System.out.println("/_/  /_/\\__,_/____/\\__/\\___/_/  /_/ /_/ /_/_/_/ /_/\\__,_/");
        System.out.println("The computer has created a four-letter code from letters A-F (e.g. 'EDEA')");
        System.out.println("You have eight guesses to crack it.");
        System.out.printf("After each guess you will receive feedback in the form of colored circles (%s or %s)%n%n", Validator.YELLOW, Validator.GREEN);
        System.out.printf("Example code: ADAE%nYour guess: BEAF%nFeedback: %s%s (A: right character, right position, E: right character, wrong position)%n", Validator.GREEN, Validator.YELLOW);
    }

    private void win() {
        System.out.println("Congratulations! You cracked the code!");
    }

    private void lose() {
        System.out.println("You lost! What a pity. Maybe give it another try...");
    }
}
