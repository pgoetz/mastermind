package de.pgoetz.mastermind;

/**
 * Validates Mastermind guesses and offers hints of (partially correct) tries.
 */
public class Validator {
    public static final String YELLOW = "\uD83D\uDFE1";
    public static final String GREEN = "\uD83D\uDFE2";
    public static final String CORRECT_GUESS = "\uD83D\uDFE2\uD83D\uDFE2\uD83D\uDFE2\uD83D\uDFE2";

    private final String code;

    /**
     * Initialize this validator with the code that has to be guessed during the game of Mastermind.
     *
     * @param code correct code that guesses will be validated against
     */
    public Validator(String code) {
        this.code = code;
    }

    /**
     * Validate the guess against the code and check if it contains correct characters. Every correct character adds
     * Validator.YELLOW to the resulting String. If the correct character sits on the right index in the guess,
     * Validator.GREEN will be added to the result.
     *
     * Example: Code "ABCD" and guess "EACE" return Validator.YELLOW (A on the wrong index)
     * and Validator.GREEN (C on the right index).
     *
     * @param guess guess that should be checked against the correct code.
     * @return String with the Validator.YELLOW and Validator.GREEN hints set; return empty String for incorrect guess
     */
    public String validate(String guess) {
        return null;
    }
}
